.. image:: https://travis-ci.org/jeremiedecock/hhunt.svg?branch=master
    :target: https://travis-ci.org/jeremiedecock/hhunt

============
House hunter
============

Copyright (c) 2015-2018 Jeremie DECOCK (http://www.jdhp.org)

* Web site: http://www.jdhp.org/projects_en.html
* Source code: https://github.com/jeremiedecock/hhunt
* Issue tracker: https://github.com/jeremiedecock/hhunt/issues
* House hunter on PyPI: https://pypi.python.org/pypi/hhunt
* House hunter on Anaconda Cloud: https://anaconda.org/jdhp/hhunt


Description
===========

House hunter is an open source tool to manage house adverts for house seekers.

.. warning::

    This project is in beta stage.


Dependencies
============

- Python >= 3.0
- Qt5 for Python
- Pandas


.. _install:

Installation
============

Gnu/Linux
---------

You can install, upgrade, uninstall House hunter with these commands (in a
terminal)::

    pip install --pre hhunt
    pip install --upgrade hhunt
    pip uninstall hhunt

Or, if you have downloaded the House hunter source code::

    python3 setup.py install

.. There's also a package for Debian/Ubuntu::
.. 
..     sudo apt-get install hhunt

Windows
-------

.. Note:
.. 
..     The following installation procedure has been tested to work with Python
..     3.4 under Windows 7.
..     It should also work with recent Windows systems.

You can install, upgrade, uninstall House hunter with these commands (in a
`command prompt`_)::

    py -m pip install --pre hhunt
    py -m pip install --upgrade hhunt
    py -m pip uninstall hhunt

Or, if you have downloaded the House hunter source code::

    py setup.py install

MacOSX
-------

Note:

    The following installation procedure has been tested to work with Python
    3.5 under MacOSX 10.9 (*Mavericks*).
    It should also work with more recent MacOSX systems.

You can install, upgrade, uninstall House hunter with these commands (in a
terminal)::

    pip install --pre hhunt
    pip install --upgrade hhunt
    pip uninstall hhunt

Or, if you have downloaded the House hunter source code::

    python3 setup.py install

House hunter requires Qt5 and its Python 3 bindings plus few additional
libraries to run.

.. These dependencies can be installed using MacPorts::
.. 
..     port install gtk3
..     port install py35-gobject3
..     port install py35-matplotlib

.. or with Hombrew::
.. 
..     brew install gtk+3
..     brew install pygobject3


Bug reports
===========

To search for bugs or report them, please use the House hunter Bug Tracker at:

    https://github.com/jeremiedecock/hhunt/issues


License
=======

This project is provided under the terms and conditions of the
`MIT License`_.

.. _MIT License: http://opensource.org/licenses/MIT
.. _House hunter: https://github.com/jeremiedecock/hhunt
.. _command prompt: https://en.wikipedia.org/wiki/Cmd.exe
